module.exports = {
    platform: 'bitbucket',
    gitAuthor: 'Renovate Bot <bot@renovateapp.com>',
    username: 'app-integrations',
    onboarding: true,
    rebaseWhen: 'behind-base-branch',
    onboardingConfig: {
      extends: ['config:base']
    },
    repositories: [
      'cloudbeds/cloudbeds-sdk',
      'cloudbeds/cloudbeds-qa-request'
    ]
  }